/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {

 // if (vhodnoBesedilo.search ("sandbox.lavbic.net/teaching/OIS/gradivo/")){
 //   return vhodnoBesedilo;
 // }
 // else {
  var izhod = vhodnoBesedilo.split(" ");
  
  //sprehodi se po arrayu izhod (vsak string posebi)
  for (i in izhod){
  if (vhodnoBesedilo.search(("http://" || "https://")  && (".png" || ".gif" || ".jpg"))) {
   // console.log('jp');
   
    // replaca html(s) in jpg png gif z img styleom (ampak izpise pred njem se originalno vhodno besedilo) (RegExp)
    izhod[i] =  izhod[i].replace(/((http|https):.*(\.jpg|\.png|\.gif))/,
               izhod[i] +  '<br><img style=\"width:200px; margin-left:20px;\" src=\"$1\">');
  
  }
  //zdruzi nazaj s presledki
  var rez = izhod.join(" ");
  }
  return rez;

 // }
 
 //if (vhodnoBesedilo.search ("sandbox.lavbic.net/teaching/OIS/gradivo/")){
 //   return vhodnoBesedilo;
 // }
}
  


  



/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeSlika = sporocilo.indexOf('<img') > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  }
  //ce je slika, jo vrne v obliki html sporocilo
  else if (jeSlika) {
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

var uporabniskaImena = [];
var vsiNadImki = [];

/* DN02 uporabniku dodaj nadimek
      */
function uporabnikuDodajNadImek(uporIme, nadImek){
  //kot boolean
  var aliPrideDoSpremembe = 0;
  
  //vzami od prve crke do predzadnje
  uporIme = uporIme.substring(1, uporIme.length-1);
  nadImek = nadImek.substring(1, nadImek.length-1);

//ce je uporabnisko ime == upor Imenu, mu dodaj nadImek
  for (var i = 0; i < uporabniskaImena.length; i++){
      if(uporabniskaImena[i]==uporIme){
        vsiNadImki[i] = nadImek;
        aliPrideDoSpremembe = 1;
      }
  }
} 

/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSlike(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  //Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    //loci po : da bo lazje za obravnavo v arrayu
    var vhod=sporocilo.besedilo.split(":");
    //varna vrednost, dodelimo kateri v tabeli ima
    var aliImaNadimek=-1000;
    
    //poglej ce ima kdo nadImek in ce je uporabnisko ime == prvi besedi v vhodu
    for(var k = 0; k < uporabniskaImena.length; k++){
      if(vsiNadImki[k]!= ""){
        if(uporabniskaImena[k]==vhod[0]){
            aliImaNadimek=k;
        }
          
      }
      
    }
    //ce ima nadimek izpisi z nadimkom in potem se sporocilo
    if(aliImaNadimek!==-1000){
		$('#sporocila').append(divElementEnostavniTekst(vsiNadImki[aliImaNadimek] +" ("+uporabniskaImena[aliImaNadimek]
                                        +")"+sporocilo.besedilo.substring(vhod[0].length,sporocilo.besedilo.length)));
        
    }
    //ce nima nadimka, poslji navadno sporocilo kot ponavadi
    else{
     var novElement = divElementEnostavniTekst(sporocilo.besedilo);
        $('#sporocila').append(novElement);
    }

  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '&#9756;') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
	  $('#seznam-uporabnikov').empty();
	    
	    for (var i=0; i < uporabniki.length; i++) {
			var prisotenVTabeli = 0;
			var kateriImaNadImek = -1000; //random 
			
			//preveri ali je uporabnik v tabeli uporabniskih imen			
			for(var j= 0; j < uporabniskaImena.length; j++){
				if (uporabniki[i] == uporabniskaImena[j])
					prisotenVTabeli = 1;
			}
			//ce v tabeli ni prisoten ga dodaj
			if (prisotenVTabeli == 0){
				uporabniskaImena.unshift(uporabniki [i]);
				vsiNadImki.unshift("");
			}
			//ce je v tabeli vsiNadImki nadimek, povej kateri v tabeli to je : K
			for (var k = 0; k < uporabniskaImena.length; k++){
				if (vsiNadImki[k]!=""){
					if (uporabniskaImena[k] == uporabniki[i]){
					kateriImaNadImek=k;
					}
				}
			}

			//izpisi uporabnisko ime z nadImkom
				if(kateriImaNadImek!== -1000){
				$('#seznam-uporabnikov').append(divElementEnostavniTekst(vsiNadImki[kateriImaNadImek]
								                      +  " ("+ uporabniskaImena[kateriImaNadImek]+ ")" ).click(function(){
		      // ce klikne mu poslji preko zasebnega sporocila znak
		      // 1: procesiraj ukaz in /zasebno
		      // 2: izpisi tistemu ki dregne, sporocilo koga je dregnil
			    klepetApp.procesirajUkaz('/zasebno "' + $(this).text() + '" "☜"');
			    $('#sporocila').append(divElementHtmlTekst("(zasebno za " + $(this).text() + " ): ☜"));

			    $('#poslji-sporocilo').focus();

		    }));
								                      
								                      
			}
			//ce noben nima nadimka, izpisi tako kot je bilo 
			else {
				$('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]).click(function(){
		      // ce klikne mu poslji preko zasebnega sporocila znak
		      // 1: procesiraj ukaz in /zasebno
		      // 2: izpisi tistemu ki dregne, sporocilo koga je dregnil
			    klepetApp.procesirajUkaz('/zasebno "' + $(this).text() + '" "☜"');
			    $('#sporocila').append(divElementHtmlTekst("(zasebno za " + $(this).text() + " ): ☜"));

			    $('#poslji-sporocilo').focus();

		    }));
			//console.log ('ni nadimkov')
			}
      
					


   	  }
  });
	  
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
